package com.example.samples.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.samples.dao.BooksMapper;
import com.example.samples.vo.BookInfo;

import java.util.List;
import java.util.Map;

/**
 * 
 * 책 등록 관련 서비스 
 *
 */
@Service
public class BookService {
	
	//의존성 주입 
	@Autowired
	BooksMapper mapper;

	// 책을 등록 한다.
	public int registerBook(BookInfo info) throws Exception{
		return mapper.registerBook(info);
	}

	public List<BookInfo> getBookList(Map<String, Object> param) throws Exception{
		return mapper.getBookList(param);
	};
	//책 한권정보 출력
	public BookInfo getBook(int bookNo) throws Exception {
			return mapper.getBook(bookNo);
	}

	//책 업데이트
	public int updateBook(BookInfo info) throws Exception {
		return mapper.updateBook(info);
	}
	//책 삭제
	public int deleteBook(int bookNo) throws Exception{
		return mapper.deleteBook(bookNo);
	}
}
