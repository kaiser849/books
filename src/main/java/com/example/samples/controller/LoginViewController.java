package com.example.samples.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/login")
public class LoginViewController {
	
	private String id = "admin";
	private String pw = "Admin1234";
	
	//로그인 화면 
	@RequestMapping(value="/loginForm")
	public ModelAndView loginForm() {
		ModelAndView view = new ModelAndView();
		view.setViewName("login/login");
		return view;
	}
	
	// 로그인 후 메인화면 
	@RequestMapping(value="/success")
	public ModelAndView successView() {
		ModelAndView view = new ModelAndView();
		view.setViewName("login/success");
		return view;
	}
	
	
	
	// 로그인 명령 수행 
	@RequestMapping(value="/proc/login")
	public String loginProc(@RequestParam(value = "account_id") String accountId,
			                             @RequestParam(value = "account_pw") String accountPw) {
		
		ModelAndView view = new ModelAndView();
		String url;
		
		//아이디랑 패스워드가 같으면 
		if(id.equals(accountId) && pw.equals(accountPw)) {
			// 성공화면으로 간다.
			//view.setViewName("login/success");
		 url  =  "forward:/book/list";
		}else {
			url = "login/fail";
		}
		
		return url;
	}
	
	
}
