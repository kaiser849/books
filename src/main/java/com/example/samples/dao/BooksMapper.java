package com.example.samples.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.samples.vo.BookInfo;

import java.util.List;
import java.util.Map;

/**
 * 
 *책 등록 매퍼
 */
@Repository
@Mapper
public interface BooksMapper {
	// 책 등록 메서드 
	int registerBook(BookInfo info) throws Exception;
	//책리스트 출력
	List<BookInfo> getBookList(Map<String, Object> param) throws Exception;
	//책 한권정보 출력
	BookInfo getBook(int bookNo) throws Exception;
	//책 업데이트
	int updateBook(BookInfo info) throws Exception;
	//책 삭제
	int deleteBook(int bookNo) throws Exception;
}
